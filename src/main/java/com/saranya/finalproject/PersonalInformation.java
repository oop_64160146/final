/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.saranya.finalproject;

import java.io.Serializable;

/**
 *
 * @author Acer
 */
public class PersonalInformation implements Serializable{

    private String firstname;
    private String surname;
    private String dateofbirth;
    private String gender;
    private int age;
    private String ethnicity;
    private String nationality;
    private String religion;
    private String status;
    private String currentaddress;
    private String district;
    private String province;
    private int zipcode;
    private int phone;

    public PersonalInformation(String firstname, String surname, String dateofbirth, String gender, int age, String ethnicity, String nationality, String religion, String status, String currentaddress, String district, String province, int zipcode, int phone) {
        this.firstname = firstname;
        this.surname = surname;
        this.dateofbirth = dateofbirth;
        this.gender = gender;
        this.age = age;
        this.ethnicity = ethnicity;
        this.nationality = nationality;
        this.religion = religion;
        this.status = status;
        this.currentaddress = currentaddress;
        this.district = district;
        this.province = province;
        this.zipcode = zipcode;
        this.phone = phone;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentaddress() {
        return currentaddress;
    }

    public void setCurrentaddress(String currentaddress) {
        this.currentaddress = currentaddress;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int getZipcode() {
        return zipcode;
    }

    public void setZipcode(int zipcode) {
        this.zipcode = zipcode;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "PersonalInformation{" + "firstname=" + firstname + ", surname=" + surname + ", dateofbirth=" + dateofbirth + ", gender=" + gender + ", age=" + age + 
                ", ethnicity=" + ethnicity + ", nationality=" + nationality + ", religion=" + religion + ", status=" + status +
                ", currentaddress=" + currentaddress + ", district=" + district + ", province=" + province + ", zipcode=" + zipcode + ", phone=" + phone + '}';
    }
    
}

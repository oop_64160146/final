/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.saranya.finalproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Acer
 */
public class WritePerson {

    public static void main(String[] args) {
        ArrayList<PersonalInformation> persons = new ArrayList();
        persons.add(new PersonalInformation("Saranya", "Mattiko", "28/12/45", "Female", 19, "Thai", "Thai", "Buddhism", "Single", "5/3 Moo10 Canton NaWangHin", "Phanatnikhom", "ChonBuri", 20140, 617267493));
        persons.add(new PersonalInformation("Sirapapha", "Mattiko", "17/02/44", "Female", 21, "Thai", "Thai", "Buddhism", "Single", "5/3 Moo10 Canton NaWangHin", "Phanatnikhom", "ChonBuri", 20140, 922489565));
        persons.add(new PersonalInformation("Chapaphat", "Mattiko", "13/07/42", "Female", 22, "Thai", "Thai", "Buddhism", "Single", "5/3 Moo10 Canton NaWangHin", "Phanatnikhom", "ChonBuri", 20140, 905503782));

        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("personalInformation.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(persons);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WritePerson.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WritePerson.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
